# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from datetime import date, timedelta
import calendar

def diff_month(d1, d2):
    months = (d1.year - d2.year)*12 + d1.month - d2.month
    if d1.day > d2.day:
        return months
    return months-1

def diff_month_today(d):
    return diff_month(date.today(),d)

def monthdelta(date, delta):
    m, y = (date.month+delta) % 12, date.year + ((date.month)+delta-1) // 12
    if not m: m = 12
    d = min(date.day, calendar.monthrange(y, m)[1])
    return date.replace(day=d,month=m, year=y)
    
def daydelta(date, delta):
    if (delta==0): return date
    return date + timedelta(days=delta)
