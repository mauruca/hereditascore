# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.db import models
from simple_history.models import HistoricalRecords
from decimal import Decimal
from .modelConst import *
from .modelPadraoDepreciacao import *
from datetime import date
from .auxDate import diff_month_today
from decimal import *
from .customFields.CharNullField import *

class Bem(models.Model):
    CONTAMINADO_ESTADO = 13
    SUCATA_ESTADO = 12
    ANTIECONOMICO_ESTADO = 11
    OCIOSO_ESTADO = 10
    OBSOLETO_ESTADO = 9
    IRRECUPERAVEL_ESTADO = 8
    RECUPERAVEL_ESTADO = 7
    PRECARIO_ESTADO = 6
    RUIM_ESTADO = 5
    REGULAR_ESTADO = 4
    BOM_ESTADO = 3
    OTIMO_ESTADO = 2
    NOVO_ESTADO = 1
    # ATENCAO: Ao alterar aqui ajustar o método "servivel" abaixo
    ESTADO_BEM = (
        (NOVO_ESTADO, 'Novo - Servível'),
        (OTIMO_ESTADO, 'Ótimo - Servível'),
        (BOM_ESTADO, 'Bom - Servível'),
        (REGULAR_ESTADO, 'Regular - Servível'),
        (RUIM_ESTADO, 'Ruim - Servível'),
        (PRECARIO_ESTADO, 'Precário - Inservível'),
        (RECUPERAVEL_ESTADO, 'Recuperável - Inservível'),
        (IRRECUPERAVEL_ESTADO, 'Irrecuperável - Inservível'),
        (OBSOLETO_ESTADO, 'Obsoleto - Inservível'),
        (OCIOSO_ESTADO, 'Ocioso - Inservível'),
        (ANTIECONOMICO_ESTADO, 'Antioeconômico - Inservível'),
        (SUCATA_ESTADO, 'Sucata - Inservível'),
        (CONTAMINADO_ESTADO, 'Contaminado - Inservível'),
    )

    DISPONIVEL_SITUACAO = 'D'
    EXTRAVIADO_SITUACAO = 'E'
    INDISPONIVEL_SITUACAO = 'I'
    EMUSO_SITUACAO = 'U'

    SITUACAO_BEM = (
        (DISPONIVEL_SITUACAO, 'Disponível'),
        (EXTRAVIADO_SITUACAO, 'Extraviado'),
        (INDISPONIVEL_SITUACAO, 'Indisponível'),
        (EMUSO_SITUACAO, 'Em uso'),
    )

    descricao = models.CharField('Descrição', max_length=500, blank=False)
    numerotombamento = models.CharField('Número Tombamento', max_length=15, unique=True, blank=False)
    dataaquisicao = models.DateField('Data Aquisicao', blank=False)
    valoraquisicao = models.DecimalField('Valor Aquisição',max_digits=20, decimal_places=2, blank=False)
    valoratual = models.DecimalField('Valor Atual',max_digits=20, decimal_places=2, blank=False)
    #classificacao
    situacao = models.CharField('Situação',max_length=1, default='D', choices=SITUACAO_BEM, blank=False)
    estado = models.IntegerField('Estado',default=1, choices=ESTADO_BEM, blank=False)
    #depreciacao
    datainiciouso = models.DateField('Data Inicio Uso', blank=True, null=True)
    padraodepreciacao = models.ForeignKey(PadraoDepreciacao, on_delete=models.PROTECT, verbose_name="Padrão Depreciação", related_name='bens', blank=True, null=True)
    valordepreciacaoacumulada = models.DecimalField('Valor Depreciação Acumulada',max_digits=20, decimal_places=2, default=Decimal('0.00'), blank=True, null=True)
    valorresidual = models.DecimalField('Valor Residual',max_digits=20, decimal_places=2, default=Decimal('0.00'), blank=True, null=True)
    dataultimadepreciacao = models.DateField('Data Última Depreciação', blank=True, null=True)
    vidautilmeses = models.IntegerField('Vida útil em meses', blank=True, null=True)
    #inventario
    rfid = models.CharField(LABEL_RFID, max_length=200, blank=True, null=True, unique=True)
    #em uso
    ativo = models.BooleanField('Ativo',default=True)
    historico = HistoricalRecords(table_name='patrimonio_historicalbem')

    def __str__(self):
        return self.descricao + '(' + str(self.id) + ')'

    def temEtiqueta(self):
        if self.rfid:
            return True
        return False

    def servivel(self):
        if self.estado in range(1,6):
            return True
        return False

    class Meta:
        verbose_name = 'Bem'
        verbose_name_plural = 'Bens'
        db_table = 'patrimonio_bem'

    def depreciar(self):
        # Obter o modelo de depreciacao associado
        if self.padraodepreciacao is None:
            return False
        if self.datainiciouso is None:
            return False
        # Verifica se já depreciou este bem
        if self.dataultimadepreciacao is None:
            if self.primeiraDepreciacao():
              #Salva dados
              self.save()
              return True
        else:
            if self.novaDepreciacao():
                #Salva dados
                self.save()
                return True

        return False

    def primeiraDepreciacao(self):
        # Obter o tempo em meses desde o inicio do uso até hoje
        md = diff_month_today(self.datainiciouso)
        # Se meses desde a ultima depreciacao for igual a zero, não deprecia
        if md < 1:
            return False
        # Obter o valor atual do bem
        va = float(self.valoratual)
        # Calcular e definir o valor residual conforme o padrão
        vr = float(self.padraodepreciacao.percentualresidual/100) * va
        # Definir a vida util em meses a partir do padrão
        vu = float(self.padraodepreciacao.vidautilmeses)
        # Se meses a depreciar forem maiores que a vida util, utilizar a vida util
        if md > vu: md = vu
        md = float(md)
        # Obter valor depreciado = ( (valor atual - valor residual) / vida util ) * meses a depreciar
        vd = ((va - vr) / vu) * md
        # Ajusta vida util removendo os meses depreciados
        vu = vu - md
        # Ajusta bem
        self.valoratual = Decimal(va - vd)
        self.valordepreciacaoacumulada = Decimal(vd)
        self.vidautilmeses = Decimal(vu)
        self.dataultimadepreciacao = date.today()
        # Aplica o valor residual pois não havia feito depreciação antes
        self.valorresidual = vr
        return True

    def novaDepreciacao(self):
        # Obter o tempo em meses desde a ultima depreciacao até hoje
        md = diff_month_today(self.dataultimadepreciacao)
        # Se meses desde a ultima depreciacao for igual a zero, não deprecia
        if md < 1:
            return False
        # Obter o valor atual do bem
        va = float(self.valoratual)
        # Obter o valor residual
        vr = float(self.valorresidual)
        # Definir a vida util em meses a partir do padrão
        vu = float(self.vidautilmeses)
        # Se meses a depreciar forem maiores que a vida util, utilizar a vida util
        if md > vu: md = vu
        md = float(md)
        # Obter valor depreciado = ( (valor atual - valor residual) / vida util ) * meses a depreciar
        vd = ((va - vr) / vu) * md
        # Ajusta vida util removendo os meses depreciados
        vu = vu - md
        # Ajusta bem
        self.valoratual = Decimal(va - vd)
        self.valordepreciacaoacumulada += Decimal(vd)
        self.vidautilmeses = Decimal(vu)
        self.dataultimadepreciacao = date.today()
        return True
