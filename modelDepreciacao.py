# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.db import models
from decimal import *
from simple_history.models import HistoricalRecords
from .modelBem import Bem
from .modelPadraoDepreciacao import PadraoDepreciacao
from .auxDate import diff_month_today
from datetime import date
from decimal import *


class Depreciacao(models.Model):
    data = models.DateField('Depreciado em', blank=False)
    bem = models.ForeignKey(Bem, on_delete=models.PROTECT, verbose_name="Bem", related_name='depreciacoes', blank=False)
    valoranterior = models.DecimalField('Valor Anterior', max_digits=20, decimal_places=2, default=Decimal('0.00'), blank=False)
    valordepreciado = models.DecimalField('Valor Depreciado', max_digits=20, decimal_places=2, default=Decimal('0.00'), blank=False)
    historico = HistoricalRecords(table_name='patrimonio_historicaldepreciacao')

    def __str__(self):
        retorno = self.bem.descricao + '(' + str(self.id) + ')' + '(em %s/%s/%s ' % (self.data.day, self.data.month, self.data.year)
        return retorno + ' de ' + str(self.valoranterior) + ' retirado ' + str(self.valordepreciado) + ')'

    class Meta:
        verbose_name = 'Depreciação'
        verbose_name_plural = 'Histórico de Depreciações'
        db_table = 'patrimonio_depreciacao'

    def fazer(self):
        dt = date.today()
        self.valoranterior = self.bem.valoratual
        if self.bem.depreciar():
            self.data = dt
            self.valordepreciado = self.valoranterior - self.bem.valoratual
            self.bem.save()
            self.save()
            return True
        return False

class Depreciar(models.Model):
    bem = models.ForeignKey(Bem, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Depreciar Bem'
        verbose_name_plural = verbose_name
        db_table = 'patrimonio_depreciar'
