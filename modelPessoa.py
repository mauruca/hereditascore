# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.db import models
from simple_history.models import HistoricalRecords
from django.contrib.auth.models import User
from .modelAux import *
from .modelConst import *
from .customFields.CharNullField import *

class PessoaFisica:
    CPF = Cpf()

class PessoaJuridica:
    CNPJ = Cnpj()

class Pessoa(models.Model):
    user = models.OneToOneField(User, on_delete=models.PROTECT, verbose_name="Usuário", primary_key=False, blank=False, related_name='pessoas')
    #inventario
    rfid = CharNullField(LABEL_RFID, max_length=200, blank=True, null=True, unique=True)
    #em uso
    ativo = models.BooleanField('Ativo',default=True)
    historico = HistoricalRecords(table_name='patrimonio_historicalpessoa')

    def __str__(self):
        return self.user.username

    def nomecompleto(self):
        return self.user.first_name + " " + self.user.last_name

    def login(self):
        return self.user.username

    class Meta:
        verbose_name = 'Pessoa'
        verbose_name_plural = 'Pessoas'
        db_table = 'patrimonio_pessoa'
