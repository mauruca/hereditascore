from django.db import models
from django.contrib.auth.models import User

# Create your models here.
from .modelPessoa import *
from .modelLocal import *
from .modelBem import *
from .modelDepreciacao import *
from .modelPadraoDepreciacao import *
from .modelInventario import *
from .movimentosBem import *
