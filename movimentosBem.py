
class MovimentosBem:
    def __init__(self):
        self.idBem = ""
        self.descricaoBem = ""
        self.nTombamentoBem = ""
        self.dataultinv = ""
        self.ultlocal = ""
        self.ultinventariante = ""
        self.datapenultinv = ""
        self.penultlocal = ""
        self.penultimoinventariante = ""

    def adicionaInventario(self, item):
        if self.datapenultinv:
            return

        if not self.idBem:
            self.idBem = item['id']
            self.descricaoBem = item['descricao']
            self.nTombamentoBem = item['numerotombamento']

        if self.dataultinv:
            if item['inventario__data'] is not None:
                self.datapenultinv = item['inventario__data']
                self.penultlocal = item['inventario__local__nome']
                if item['inventario__user__first_name']:
                    self.penultimoinventariante = str(item['inventario__user__first_name']) + ' ' + str(item['inventario__user__last_name'])
            return

        if item['inventario__data'] is not None:
            self.dataultinv = item['inventario__data']
            self.ultlocal = item['inventario__local__nome']
            if item['inventario__user__first_name']:
                self.ultinventariante = str(item['inventario__user__first_name']) + ' ' + str(item['inventario__user__last_name'])
