# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.db import models
from hereditascore.models import Depreciacao, PadraoDepreciacao, Bem
from hereditascore.auxDate import diff_month_today
from datetime import date

class DepreciacaoService:

    ## Construtor da classe
    def __init__(self, listaBens = None):
        if listaBens is None:
            self.bens = []
        else:
            self.bens = list(listaBens)

    ## Deprecia uma lista de bens recebidos pelo construtor da classe
    def depreciarLista(self):
        a = iter(self.bens)
        listaRetorno = []
        lista = []

        for b in a:
            d = Depreciacao()
            d.bem = b
            if d.fazer():
                lista.append(b)
                lista.append(d)

            if len(lista) > 0:
                listaRetorno.append(lista)

            lista = []
        return listaRetorno
