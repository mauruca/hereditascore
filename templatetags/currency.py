from django import template
import locale
locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')
register = template.Library()

@register.filter()
def currency(value):
    if not value:
      return locale.currency(0, grouping=True)
    return locale.currency(value, grouping=True)
