# Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
# Ver arquivo LICENSE para os detalhes.

from django.test import TestCase, tag

from hereditascore.models import Bem, PadraoDepreciacao, Depreciacao
from hereditascore.services import DepreciacaoService
from hereditascore.auxDate import *
from datetime import date, timedelta


class DepreciacaoServiceTest(TestCase):
    def setUp(self):
        PadraoDepreciacao.objects.create(
            nome='moveis',
            vidautilmeses=12,
            percentualresidual=10)
        self.criaBemCompleto()
        self.criaBemCompleto2()
        self.criaBemCompletoRecente()
        self.criaBemCompletoRecente1Mes()
        self.criaBemSempadrao()
        self.criaBemDepreciado1()

    def criaBem(self, desc, tombamento, datainicio, prfid):
        return Bem(
            descricao = desc,
            numerotombamento = tombamento,
            rfid = prfid,
            dataaquisicao = date(2007, 12, 5),
            datainiciouso = datainicio,
            valoraquisicao = 1000.00,
            valoratual = 1000.00,
        )

    def criaBemDepreciado(self, desc, tombamento, ultimadepreciacao, vida, residual, prfid):
        return Bem(
            descricao = desc,
            numerotombamento = tombamento,
            rfid = prfid,
            dataaquisicao = date(2007, 12, 5),
            datainiciouso = date(2007, 12, 5),
            valoraquisicao = 1000.00,
            valoratual = 1000.00,
            dataultimadepreciacao = ultimadepreciacao,
            vidautilmeses = vida,
            valorresidual = residual,

        )

    def criaBemCompleto(self):
        bem = self.criaBem('teste bem',111,date(2008, 1, 15), '123')
        bem.padraodepreciacao = PadraoDepreciacao.objects.get(nome='moveis')
        bem.save()
        return bem

    def criaBemCompleto2(self):
        bem = self.criaBem('teste bem 2',222,date(2015, 10, 15), '1234')
        bem.padraodepreciacao = PadraoDepreciacao.objects.get(nome='moveis')
        bem.save()
        return bem

    def criaBemCompletoRecente(self):
        bem = self.criaBem('teste bem recente 3',333,daydelta(date.today(), -5), '12345')
        bem.padraodepreciacao = PadraoDepreciacao.objects.get(nome='moveis')
        bem.save()
        return bem

    def criaBemCompletoRecente1Mes(self):
        bem = self.criaBem('teste bem 4',444,(monthdelta(date.today(), -1) - timedelta(days=1)), '123456')
        bem.padraodepreciacao = PadraoDepreciacao.objects.get(nome='moveis')
        bem.save()
        return bem

    def criaBemSempadrao(self):
        bem = self.criaBem('teste bem sem padrao',555,date(2008, 1, 15), '1234567')
        bem.save()
        return bem

    def criaBemDepreciado1(self):
        bem = self.criaBemDepreciado('teste bem depreciado 1',666,date(2015, 7, 15), 20, 50, '12345678')
        bem.padraodepreciacao = PadraoDepreciacao.objects.get(nome='moveis')
        bem.save()
        return bem

    def criaLista1Item(self):
        lista = []
        lista.append(Bem.objects.get(descricao='teste bem'))
        return lista

    def criaLista2Itens1Ruim(self):
        lista = []
        lista.append(Bem.objects.get(descricao='teste bem'))
        lista.append(Bem.objects.get(descricao='teste bem sem padrao'))
        return lista

    def criaLista2Itens1DataRecente(self):
        lista = []
        lista.append(Bem.objects.get(descricao='teste bem recente 3'))
        lista.append(Bem.objects.get(descricao='teste bem sem padrao'))
        return lista

    def criaLista2Itens1DataRecenteMaisDe1Mes(self):
        lista = []
        lista.append(Bem.objects.get(descricao='teste bem 4'))
        lista.append(Bem.objects.get(descricao='teste bem sem padrao'))
        return lista

    def criaLista2Itens(self):
        lista = []
        lista.append(Bem.objects.get(descricao='teste bem'))
        lista.append(Bem.objects.get(descricao='teste bem 2'))
        return lista

    @tag('slow', 'core')
    def test_deprecia_bem_valorresidual(self):
        "deprecia um bem completamente até o valor residual"
        bem = Bem.objects.get(descricao='teste bem')
        dep = Depreciacao()
        dep.bem = bem
        dep.fazer()
        self.assertEqual(bem.valoratual, bem.valorresidual)

    @tag('slow', 'core')
    def test_naodeprecia_bem_sem_padrao(self):
        bem = Bem.objects.get(descricao='teste bem sem padrao')
        dep = Depreciacao()
        dep.bem = bem
        self.assertFalse(dep.fazer())

    @tag('slow', 'core')
    def test_deprecialista_1_item(self):
        ds = DepreciacaoService(self.criaLista1Item())
        ds.depreciarLista()
        self.assertEqual(len(ds.bens), 1)
        self.assertEqual(
            ds.bens[0].valoratual,
            Depreciacao.objects.get(bem=ds.bens[0]).valoranterior - Depreciacao.objects.get(bem=ds.bens[0]).valordepreciado)

    @tag('slow', 'core')
    def test_deprecialista_2_itens_1_sem_padrao_nao_deprecia(self):
        ds = DepreciacaoService(self.criaLista2Itens1Ruim())
        lista = ds.depreciarLista()
        self.assertEqual(len(lista), 1)

    @tag('slow', 'core')
    def test_deprecialista_2itens_1datarecente_e_1sempadrao_naodeprecia(self):
        ds = DepreciacaoService(self.criaLista2Itens1DataRecente())
        lista = ds.depreciarLista()
        self.assertEqual(len(lista), 0)

    @tag('slow', 'core')
    def test_deprecialista_2it_1datarec1m1d_e_1sempadrao_1item_deprecia(self):
        ds = DepreciacaoService(self.criaLista2Itens1DataRecenteMaisDe1Mes())
        lista = ds.depreciarLista()
        self.assertEqual(len(lista), 1)

    @tag('slow', 'core')
    def test_deprecialista_2_itens(self):
        ds = DepreciacaoService(self.criaLista2Itens())
        lista = ds.depreciarLista()
        self.assertEqual(len(lista), 2)

    @tag('slow', 'core')
    def test_deprecia_bem_depreciado(self):
        bem = Bem.objects.get(descricao='teste bem depreciado 1')
        dep = Depreciacao()
        dep.bem = bem
        va = bem.valoratual
        dep.fazer()
        self.assertEqual(va-bem.valoratual,dep.valordepreciado)
